<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Blog;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * All blogs the User has created.
    * A user can have many blogs.
    * @return pivot info
    */
    public function blogs()
    {
        return $this->belongsToMany(Blog::class, 'user_blogs')
            ->withTimestamps();
    }

    /**
    * Add a blog to the user's list of blogs
    *
    * @param $blog
    */
    public function addBlog(Blog $blog)
    {
        $this->blogs()->attach($blog);
    }

    public function rmBlog(Blog $blog)
    {
        $this->blogs()->detach($blog);
    }
}
