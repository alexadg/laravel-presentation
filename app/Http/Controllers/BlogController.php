<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Blog;
use App\User;
use Auth;

class BlogController extends Controller
{
	/**
	* Save a blog post, or return view to create one
	*
	* @return $view
	*/
    public function create(Request $request)
    {
    	if ($request->isMethod('POST'))
    	{
            $blog = new Blog;
    		$blog->name = $request->get('name');
            $blog->post = $request->get('post');
            $blog->save();

            //Now link the blog to a user
            $user = Auth::user();
            $user->addBlog($blog);

            //return to what blog post lookslike
            $view = Redirect('/blog/'.$blog->id);
    	}
    	else
    	{
    		$view = view('blog.create');
    	}
    	return $view;
    }

    public function view(Blog $blog)
    {
        return view('blog.view', ['blog' => $blog]);
    }
}
