# Getting Started

1. Install WAMP
	1. Click on WAMP tray icon > phpmyadmin
	2. Login with user: "root" pass: ""
	3. On side nav click _new+_
	4. Add database "blog"
2. Install Composer
3. Clone this repo into C:\wamp\www
4. `cd blog`
5. `composer install`
6. `php artisan migrate`
7. Click on WAMP tray icon > Apache > Alias directories > Add an alias
8. `blog`
9. `C:\wamp\www\blog\public`
10. Ensure browsing to *localhost/blog/* works.

_If not already done:_
1. Go to C:\wamp\www\blog\public\.htaccess
2. Below line `RewriteEngine On` add `RewriteBase /blog/`

**Warning for Voyager**
Since you cannot set up an alias to Voyager, you will likely need to set your .htaccess to be `/blog/public` instead.

Feel free to play with this Repository as you wish.



# What we did:

install in c:/wamp/www

add to Apache Alias

.htaccess

    >RewriteBase /blog/

php artisan make:auth

	>Re-does some views, that's why you do it now (before starting code).
	>Makes login and registration routes and functions, DONE

Database

	>go to phpmyadmin
	>add database blog

.env

	>remove from .gitignore and ensure mysql

Change the views

	- abstract/separate things out more
	- move navbar to separate blade

Create a Blog 

	- add nav link to href="{{url("/blog/create")}}"
	- php artisan make:controller BlogController
	- add to route
	- add view
	- php artisan make:model Blog -m
	- php artisan make:model UserBlog -m
	- add function to User model
	- php artisan migrate
	- test!
		
List of Blogs

	- just using home dashboard now
	- edit controller
	- use blade loop




#default readme below:
# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
