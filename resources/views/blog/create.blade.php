@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Enter a blog post</div>
                <div class="panel-body">
                    <form method="POST" action="{{url("/blog/create")}}" class="form-horizontal">
                        {{ csrf_field() }}
                    	<div class="form-group">
                            <label for="name" class="control-label col-md-2">blog name</label>
                            <div class="col-md-10">
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                        </div>

                    	<div class="form-group">
                            <label for="post" class="control-label col-md-2">blog post</label>
                            <div class="col-md-10">
                                <textarea name="post" id="post" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
